import * as mongoose from 'mongoose';
const { Schema } = mongoose;

export const UserSchema = new Schema({
    name: String,
    gender: String,
    email: String,
    mobile: String,
    category: String,
    profile_pic: String
})
