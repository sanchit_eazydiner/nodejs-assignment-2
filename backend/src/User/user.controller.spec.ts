import {Test} from '@nestjs/testing';
import { CreateUserDTO } from './dto/user.dto';
import {UserController} from './user.controller';
import {UserService} from './user.service';

describe('User Controller',()=>{
    let userController: UserController;
    let userSrv: UserService;

    const mockUserService = {
        User: jest.fn(),
    };
    
    const mockResponse = {
        res: jest.fn(),
    };

    beforeEach(async()=>{
        const module = await Test.createTestingModule({
            controllers:[UserController],
            providers:[
                {
                    provide:UserService,
                    useValue:{
                        getAllUsers: jest.fn().mockReturnValue([
                            new CreateUserDTO('test-name','test-gender','test-email','test-mobile','test-category'
                            ,'test-profile-pic'),
                            new CreateUserDTO('test-name-2','test-gender-2','test-email-2','test-mobile-2','test-category-2'
                            ,'test-profile-pic-2'),
                        ]),

                        createAUser: jest.fn().mockImplementation((user:CreateUserDTO)=>
                        Promise.resolve({_id:'a uuid',...user})),

                        updateUser: jest.fn().mockImplementation((user:CreateUserDTO)=>
                        Promise.resolve({_id:'a uuid',...user})),

                        deleteAUser: jest.fn().mockResolvedValue({_id:'a uuid'}),
                    }
                }
            ]})
        .compile();

        userSrv=module.get<UserService>(UserService);
        userController=module.get<UserController>(UserController);
    });

    
    it('should be defined', () => {
        expect(userController).toBeDefined();
        });
    describe('getAllUsers',()=>{
        it('should return array of entries',async() => {
            expect(userController.getAllUsers(mockResponse)).resolves.toEqual([
                {
                    name: 'test-name',
                    gender: 'test-gender',
                    email: 'test-email',
                    mobile:'test-mobile',
                    category:'test-category',
                    profile_pic:'test-profile-pic',
                },
                {
                    name:'test-name-2',
                    gender:'test-gender-2',
                    email:'test-email-2',
                    mobile:'test-mobile-2',
                    category:'test-category-2',
                    profile_pic:'test-profile-pic-2',
                },
            ]);
        });
    });
    describe(' a new user',()=>{
        it('should create a new user',async()=>{
            const userDTO:CreateUserDTO = {
                name: 'testname 1',
                email: 'testemail 1',
                gender: 'testgender 1',
                mobile:'testmobile 1',
                category:'testCategory 1',
                profile_pic:'testProfilePice 1'
            };
            expect(userController.createAUser(mockResponse,userDTO)).resolves.toEqual({
                _id:'a uuid',
                ...userDTO,
            });
        });
    });
    describe('update a user',()=>{
        it('should update the user',async()=>{
            const userDTO:CreateUserDTO = {
                name:'new testname',
                email:'new testemail',
                gender:'new testgender',
                mobile:'new mobile',
                category:'new category',
                profile_pic:'new profilepic'
            };
            expect(userController.updateUser(mockResponse,userDTO,'a uuid')).resolves.toEqual({
                _id:'a uuid',
                ...userDTO,
            });
        });
    });
    describe('delete a user',()=>{
        it('should delete the user and return it',async()=>{
            expect(userController.deleteUser(mockResponse,'a uuid that exists')).resolves.toEqual({
                deleted: true,
            });
        });
        it('should return that it did not delete a cat', () => {
            const deleteSpy = jest
            .spyOn(userSrv, 'deleteAUser')
            .mockResolvedValueOnce({ deleted: false });
            expect(
            userController.deleteUser(mockResponse,'a uuid that does not exist'),
            ).resolves.toEqual({ deleted: false });
            expect(deleteSpy).toBeCalledWith('a uuid that does not exist');
        });
    });
});