import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interface/user.interface'
import { CreateUserDTO } from './dto/user.dto';
import { UserInt} from './interface/userInt.interface'

@Injectable()
export class UserService {
    constructor(@InjectModel("User") private readonly userModel: Model<User>) { }

    async createUser(createUserDTO: CreateUserDTO): Promise<UserInt> {
        const newUser = await this.userModel.create(createUserDTO as any);
        return newUser;
    }

    async getAllUser(): Promise<User[]> {
        const user = await this.userModel.find().exec();
        return user;
    }

    async updateAUser(_id, createUserDTO: CreateUserDTO): Promise<User> {
        const user = await this.userModel.findByIdAndUpdate(_id, createUserDTO, { new: true });
        return user;
    }

    async deleteAUser(_id): Promise<{deleted:boolean;User?}> {
        try
        {
        const deletedUser = await this.userModel.remove({_id});
        return {deleted:true,User:deletedUser};
        }
        catch (err)
        {
            return {deleted:false};
        }
    }
}
