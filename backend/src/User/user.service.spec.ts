import {Test, TestingModule} from '@nestjs/testing';
import { UserService } from './user.service';
import {createMock} from '@golevelup/nestjs-testing'
import { getModelToken } from '@nestjs/mongoose';
import {User} from './interface/user.interface';
import { Model,Query } from 'mongoose';
import {UserInt} from './interface/userInt.interface'

const mockUser = (
    name = 'test-name',
    email = 'test-email',
    gender = 'test-gender',
    mobile = 'test-mobile',
    category = 'test-category',
    profile_pic = 'test-profile-pic',
): UserInt => ({
    name,
    email,
    gender,
    mobile,
    category,
    profile_pic,
});

const mockUserDoc = (mock?: Partial<UserInt>): Partial<User> => ({
    name: mock?.name || 'test-name',
    email: mock?.email || 'test-email',
    gender: mock?.gender || 'test-gender',
    mobile: mock?.mobile || 'test-mobile',
    category: mock?.category || 'test-category',
    profile_pic: mock?.profile_pic || 'test-profile-pic',
});

const userArray = [
    mockUser(),
    mockUser('mike','mike@gmail.com','male','4545676789','java','..'),
    mockUser('juliet','juliet@yahoo.com','female','2323454567','c++','..'),
];

const userDocArray = [
    mockUserDoc(),
    mockUserDoc({ 
        name: 'mike', 
        email: 'mike@gmail.com',
        gender: 'male',
        mobile: '4545676789',
        category:   'java', 
        profile_pic:'..'
    }),
    mockUserDoc({ 
        name: 'juliet', 
        email: 'juliet@yahoo.com',
        gender: 'female',
        mobile: '2323454567',
        category:   'c++', 
        profile_pic:'..'
    }),
];

describe('testing user service', () => {

    let userService: UserService;
    let model: Model<User>;
    
    beforeEach(async ()=> {
        const module:TestingModule = await Test.createTestingModule({
            providers:[
                UserService,
                {
                    provide:getModelToken('User'),
                    useValue: {
                        new: jest.fn().mockResolvedValue(mockUser()),
                        constructor: jest.fn().mockResolvedValue(mockUser()),
                        find: jest.fn(),
                        update: jest.fn(),
                        create: jest.fn(),
                        remove: jest.fn(),
                        exec: jest.fn(),
                    }
            }],
        }).compile();

        userService = module.get<UserService>(UserService);
        model = module.get<Model<User>>(getModelToken('User'));
    });

    it('should be defined', () => {
        expect(userService).toBeDefined();
        });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should get all Users and return them', async () => {
        jest.spyOn(model, 'find').mockReturnValue({
            exec: jest.fn().mockResolvedValueOnce(userDocArray),
        } as any);
        const user = await userService.getAllUser();
        expect(user).toEqual(userArray);
    });

    it('should insert a new User', async () => {
        jest.spyOn(model, 'create').mockImplementationOnce(() =>
            Promise.resolve({
                name:'oliver',
                email:'oliver@gmail.com',
                gender:'male',
                mobile:'9900990099',
                category:'C', 
                profile_pic:'..'
            }),
        );
        const newUser = await userService.createUser({
            name:'oliver',
            email:'oliver@gmail.com',
            gender:'male',
            mobile:'9900990099',
            category:'C', 
            profile_pic:'..'
        });
        expect(newUser).toEqual(mockUser('oliver','oliver@gmail.com','male','9900990099','C', '..'));
    });
    
    it.skip('should update a cat successfully', async () => {
        jest.spyOn(model, 'findByIdAndUpdate').mockReturnValueOnce(
            createMock<Query<User, User>>({
            exec: jest.fn().mockResolvedValueOnce({
                _id: 'a-uuid',
                name: 'Garfield',
                email:'Garfield@gmail.com',
                gender:'male',
                mobile:'1020304050',
                category:'python', 
                profile_pic:'..'
                }),
            }),
        );
        const updatedCat = await userService.updateAUser('a-uuid',{
            name: 'Garfield',
            email:'Garfield@gmail.com',
            gender:'male',
            mobile:'1020304050',
            category:'python', 
            profile_pic:'..'                    
        });
        expect(updatedCat).toEqual(mockUser('Garfield','Garfield@gmail.com','male','1020304050','python','..'));
    });

    it('should delete a user successfully', async () => {
        jest.spyOn(model, 'remove').mockResolvedValueOnce(true as any);
        expect(await userService.deleteAUser('a bad id')).toEqual({ deleted: true,User: true});
        });
        
    it('should not delete a user', async () => {
        jest.spyOn(model, 'remove').mockRejectedValueOnce(new Error('Bad delete'));
        expect(await userService.deleteAUser('a bad id')).toEqual({
            deleted: false
        });
    });
});
