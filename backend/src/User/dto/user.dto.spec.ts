import { CreateUserDTO} from './user.dto'

describe('Create User DTO', () => {
    it('should create a User DTO object', () => {
        expect(new CreateUserDTO('test-name','test-gender','test-email','test-mobile','test-category'
        ,'test-profile-pic'))
        .toEqual(new CreateUserDTO('test-name','test-gender','test-email','test-mobile','test-category'
        ,'test-profile-pic'),);
    });
});