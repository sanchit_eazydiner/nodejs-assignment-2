export class CreateUserDTO {
    name: string;
    gender: string;
    email: string;
    mobile: string;
    category: string;
    profile_pic: string;

constructor(name: string, gender: string, email: string, mobile: string, category: string,
    profile_pic: string)
    {
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.mobile = mobile;
        this.category = category;
        this.profile_pic = profile_pic;
    }
}