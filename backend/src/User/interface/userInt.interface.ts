export interface UserInt {
    name: String,
    gender: String,
    email: String,
    mobile: String,
    category: String,
    profile_pic: String
}