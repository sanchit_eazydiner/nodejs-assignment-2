import { Document } from 'mongoose';

export interface User extends Document {
    readonly name: String,
    readonly gender: String,
    readonly email: String,
    readonly mobile: String,
    readonly category: String,
    readonly profile_pic: String
}
