import { Controller, Res, HttpStatus, Post, Get, Param, Body, Patch, Delete, Put } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDTO } from './dto/user.dto';

@Controller('user')
export class UserController {
    constructor(private userService: UserService){}

    @Post('/add')
    async createAUser(@Res() res, @Body() createUserDTO: CreateUserDTO) {
        const user = await this.userService.createUser(createUserDTO);
        return res.status(HttpStatus.CREATED).json({
            status: 201,
            message: "Successful!",
            data: user
        })
    }

    @Get('/all')
    async getAllUsers(@Res() res) {
        const user = await this.userService.getAllUser();
        return res.status(HttpStatus.OK).json({
            status: 200,
            data: user
        })
    }

    @Put('/update/:userId')
    async updateUser(@Res() res, @Body() createUserDTO: CreateUserDTO, @Param("userId") _id: string) {
        const user = await this.userService.updateAUser(_id, createUserDTO);
        if (!user)
            return res
                .status(HttpStatus.NOT_FOUND)
                .json({ status: 404, error: "Not found!" });
        return res.status(HttpStatus.OK).json({
            status: 200,
            message: 'Successful!',
            user
        });
    }

    @Delete('/delete/:userId')
    async deleteUser(@Res() res, @Param('userId') _id: string):Promise<{ deleted: boolean }> {
        const user = await this.userService.deleteAUser(_id);
        if (!user)
            return res
                .status(HttpStatus.NOT_FOUND)
                .json({ status: 404, error: "Not found!" });
        return res.status(HttpStatus.OK).json({
            status: 200,
            message: 'Successful!',
        })
    }

}
